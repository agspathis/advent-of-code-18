(defpackage #:aoc18
  (:use #:cl #:alexandria #:cl-ppcre))

(in-package #:aoc18)

(defmacro -> (&rest forms)
  (when forms
    `(let* ,(mapcar (lambda (form) (list '<> form)) forms) <>)))

;;; day 1
(defun p1-1 () (reduce #'+ (input-lines "input-1" #'parse-integer)))

(defun p1-2 ()
  (let ((ht (make-hash-table))
	(freq 0))
    (dolist (n (join-ends (input-lines "input-1" #'parse-integer)))
      (incf freq n)
      (if (gethash freq ht)
	  (return freq)
	  (setf (gethash freq ht) t)))))

;;; day 2
(defun p2-1 ()
  (-> (lambda (line)
	(-> (frequencies line)
	    (mapcar #'cdr <>)
	    (remove-if-not (lambda (f) (or (= f 2) (= f 3))) <>)
	    (remove-duplicates <>)))
      (input-lines "input-2" <>)
      (reduce #'append <>)
      (* (count 2 <>) (count 3 <>))))

(defun string-diffs (s1 s2)
  "Assuming that S1 and S2 are strings of equal lengths, count the different
  characters between them (case-sensitive)."
  (declare (type string s1 s2))
  (assert (= (length s1) (length s2)))
  (let ((diff-count 0))
    (dotimes (i (length s1) diff-count)
      (when (char/= (aref s1 i) (aref s2 i))
	(incf diff-count)))))

(defun keep-common (s1 s2)
  "Assuming that S1 and S2 are strings of equal lengths, keep only the common
  characters between them (per index)."
  (declare (type string s1 s2))
  (assert (= (length s1) (length s2)))
  (let ((result))
    (dotimes (i (length s1) (coerce (reverse result) 'string))
      (when (char= (aref s1 i) (aref s2 i))
	(push (aref s1 i) result)))))

(defun p2-2 ()
  (-> (input-lines "input-2")
      (combinations <> 2)
      (find 1 <> :key (lambda (ids) (apply #'string-diffs ids)))
      (apply #'keep-common <>)))

;;; day 3
;;; #650 @ 226,895: 11x28
(defun parse-claim (claim)
  (-> "#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)"
      (create-scanner <>)
      (multiple-value-bind (start end inits terms) (scan <> claim)
	(declare (ignore start end))
	(map 'list (lambda (init term) (subseq claim init term)) inits terms))
      (mapcar #'parse-integer <>)))

(defun mark-claim (fabric claim)
  (destructuring-bind (id origin-x origin-y size-x size-y) claim
    (loop :for x :from origin-x :to (+ origin-x size-x -1)
	  :do (loop :for y :from origin-y :to (+ origin-y size-y -1)
		    :do (push id (aref fabric x y))))))

(defun p3-1 ()
  (let ((fabric (make-array '(1000 1000)
			    :element-type 'list
			    :initial-element ())))
    (-> (input-lines "input-3" #'parse-claim)
	(mapc (lambda (claim) (mark-claim fabric claim)) <>))
    (let ((result 0))
      (dotimes (i (array-total-size fabric) result)
	(when (> (length (row-major-aref fabric i)) 1)
	  (incf result))))))

;;; 235
(defun p3-2 ()
  (let ((fabric (make-array '(1000 1000)
			    :element-type 'list
			    :initial-element ())))
    (let* ((claims (input-lines "input-3" #'parse-claim))
	   (claim-ids (mapcar #'car claims)))
      (mapc (lambda (claim) (mark-claim fabric claim)) claims)
      (dotimes (i (array-total-size fabric) (car claim-ids))
	(when (> (length (row-major-aref fabric i)) 1)
	  (mapc (lambda (claim-id)
		  (setf claim-ids (remove claim-id claim-ids)))
		(row-major-aref fabric i)))))))

;;; day 4
;;; 21956
(defun p4-1 ()
  (let* ((schedule (-> (input-lines "input-4")
		       (sort <> #'string<)
		       (group-shifts <>)
		       (reduce #'update-schedule <> :initial-value nil)))
	 (sleepiest-guard (find-sleepiest-guard schedule)))
    (* sleepiest-guard
       (find-sleepiest-minute schedule sleepiest-guard))))

;;; 134511
(defun p4-2 ()
  (let* ((schedule (-> (input-lines "input-4")
		       (sort <> #'string<)
		       (group-shifts <>)
		       (reduce #'update-schedule <> :initial-value nil)))
	 (most-slept-minute (find-most-slept-minute schedule)))
    (destructuring-bind (minute times-asleep) most-slept-minute
      (* minute (find-most-slept-guard schedule times-asleep)))))

(defun begin-note-p (note) (scan "begins shift" note))
(defun wakeup-note-p (note) (scan "wakes up" note))
(defun asleep-note-p (note) (scan "falls asleep" note))

(defun get-guard (note)
  (-> (create-scanner "(?<=#)\\d+")
      (scan-to-strings <> note)
      (parse-integer <>)))

(defun get-minute (note)
  (-> (create-scanner "\\d{2}(?=])")
      (scan-to-strings <> note)
      (parse-integer <>)))

(defun get-guard+minutes (shift)
  (destructuring-bind (shift-start &rest sleeps) shift
    (list* (get-guard shift-start)
	   (mapcar #'get-minute sleeps))))

(defun group-shifts (notes &optional (shifts nil))
  (if (null notes)
      (-> shifts
	  (mapcar #'nreverse <>)
	  (nreverse <>)
	  (mapcar #'get-guard+minutes <>))
      (destructuring-bind (note &rest rest-notes) notes
	(if (begin-note-p note)
	    (push (list note) shifts)
	    (push note (car shifts)))
	(group-shifts rest-notes shifts))))

(defun update-schedule (schedule shift)
  (destructuring-bind (guard &rest minutes) shift
    (setf (getf schedule guard)
	  (mark-ranges (or (getf schedule guard)
			   (apply #'vector (make-list 60 :initial-element 0)))
		       minutes))
    schedule))

(defun mark-ranges (schedule minutes)
  (if (> (length minutes) 1)
      (progn (destructuring-bind (init term &rest rest) minutes
	       (loop :for i :from init :below term :do (incf (svref schedule i)))
	       (mark-ranges schedule rest)))
      schedule))

(defun find-sleepiest-guard (schedule)
  (let* ((totals (-> schedule
		     (remove-if-not #'vectorp <>)
		     (mapcar (lambda (v) (reduce #'+ v)) <>)))
	 (max (apply #'max totals))
	 (pos (position max totals)))
    (nth (* pos 2) schedule)))

(defun find-sleepiest-minute (schedule guard)
  (let* ((guard-schedule (getf schedule guard))
	 (max (apply #'max (or (coerce guard-schedule 'list) '(0)))))
    (position max guard-schedule)))

(defun index-max (seq)
  (let ((i-max 0) (v-max most-negative-fixnum))
    (dotimes (i (length seq) (list i-max v-max))
      (when (> (elt seq i) v-max)
	(setf i-max i v-max (elt seq i))))))

(defun find-most-slept-minute (schedule)
  (-> schedule
      (remove-if-not #'vectorp <>)
      (mapcar #'index-max <>)
      (sort <> #'> :key #'cadr)
      (car <>)))

(defun find-most-slept-guard (schedule minute)
  (-> schedule
      (remove-if-not #'vectorp <>)
      (mapcar (lambda (v) (= minute (apply #'max (coerce v 'list)))) <>)
      (position t <>)
      (nth (* <> 2) schedule)))

;;; day 5
(defparameter reaction-scanner
  (flet ((make-reacting-pair (code)
	   (let ((up (code-char code))
		 (low (code-char (+ code 32))))
	     (coerce (list up low #\| low up) 'string ))))
    (-> (loop :for code :from 65 :to 90
	      :collect (make-reacting-pair code))
	(reduce (lambda (pairs new-pair)
		  (concatenate 'string pairs "|" new-pair))
		<>)
	(create-scanner <>))))

;;; 11118
(defun p5-1 ()
  (do* ((last (car (input-lines "input-5")) new)
	(new  (regex-replace-all reaction-scanner last "")
	      (regex-replace-all reaction-scanner last "")))
       ((string= last new) (length new))))

(defun polymer-length-without-char (char)
  (do* ((last (remove-if (lambda (c) (char-equal c char))
			 (car (input-lines "input-5")))
	      new)
	(new  (regex-replace-all reaction-scanner last "")
	      (regex-replace-all reaction-scanner last "")))
       ((string= last new) (length new))))

;;; 6948
(defun p5-2 ()
  (-> (loop for code :from 65 :to 90 :collect code)
      (mapcar #'code-char <>)
      (mapcar (lambda (char)
		(sb-thread:make-thread
		 (lambda ()
		   (polymer-length-without-char char))))
	      <>)
      (mapcar #'sb-thread:join-thread <>)
      (apply #'min <>)))

;;; Utilities
(defun make-input-pathname (filename)
  (merge-pathnames
   (make-pathname :directory '(:relative "inputs")
		  :name filename)))

(defmacro with-input (filename &body body)
  `(with-open-file (input (make-input-pathname ,filename) :direction :input)
     ,@body))

(defun input-lines (filename &optional (parser #'identity))
  (with-input filename
    (loop for line = (read-line input nil)
	  while line
	  collect (funcall parser line))))

(defun join-ends (list) (apply #'circular-list list))

(defun frequencies (seq)
  (let ((fs))
    (dolist (item (coerce seq 'list) fs)
      (let ((f (assoc item fs)))
	(if f
	    (incf (cdr (assoc item fs)) 1)
	    (setf fs (acons item 1 fs)))))))

;;; From stack overflow
(defun cartesian-product (&rest lists)
  "Compute the cartesian product of the elements in LISTS."
  (if (car lists)
      (mapcan (lambda (inner-val)
                (mapcar (lambda (outer-val)
                          (cons outer-val
				inner-val))
                        (car lists)))
              (apply #'cartesian-product (cdr lists)))
      (list nil)))

;;; From rosetta code
(defun combinations (list m)
  "Take the combinations of elements in LIST by M."
  (let ((result))
    (labels ((combine (l c m)
	       (when (>= (length l) m)
		 (when (zerop m)
		   (return-from combine (push c result)))
		 (combine (cdr l) c m)
		 (combine (cdr l)
			  (cons (car l) c)
			  (1- m)))))
      (combine list nil m)
      result)))
